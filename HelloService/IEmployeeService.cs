﻿using System.ServiceModel;

namespace EmployeeService
{
    [ServiceContract]
    public interface IEmployeeService
    {
        [OperationContract]
        Employee GetEmployee(int Id);

        [OperationContract]
        void SaveEmployee(Employee Employee);
    }
    #region Sample code
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IHelloService" in both code and config file together.
    //[ServiceContract (Name = "IHelloService")]//provides the actual name using name parameter so we can change the name of interface without affecting the client who are using this service
    //public interface IHelloServiceChanged//you can rename it to anything it wont affect the working of existing service 
    //{
    //    [OperationContract]
    //    string GetMessage(string name);
    //}

    //[ServiceContract]
    //public interface IPrintService
    //{
    //    [OperationContract]
    //    string PrintMessage(string name);
    //}
    #endregion

}
